# GSA17 - GNSS State of the Art 2017
<div align="center">
 <img src="/uploads/f06ae7dcb33b3203e81afd5622a74e6b/Logo_IP_GSA17.png" width=221></img> 
</div>
 
<br/>
This is a project being developed for the I3 project course at the University of Salzburg as part of the M.Sc Applied Geoinformatics program.
<br/><br/>
Accomplices: *A. Lang*

## Short Abstract about Project

The  connecting  element  of  smart  watches,  smart  phones  and  geodetic  measurement 
tools  is  the  existence  of  a  GNSS  module.  By  terms  of  definition  they  are  equal,  all  of 
them  use  the  information  provided  by  satellites  to  estimate  the  position  on  the  earth 
surface.  Considering  the  functionality  and  capability  there  are  marked  differences  in 
cost,  accuracy  and  precision.  The  variety  of  different  receivers,  antennas  and  its 
software  and  hardware  components  makes  the  identification  of  the  right  product  for  a 
special need challenging. Technology trends establish a continuous overflow of available 
GNSS modules with different technical spreadsheets on the market. This paper gives an 
overview  of  the  state  of  the  art  GNSS  modules  available  on  the  market  in  2017  and 
identifies potentials and drawbacks of a selected group of devices. For this project three 
devices  will  be  tested  and  validated:  an  Android  GPS  device  (with  RAW  GPS 
functionality), a low cost single frequency GPS module and a low cost two-frequency GPS 
module.  The  receivers  are  configured  under  best  practice  aspects  and  tested  with  a 
uniform testing cycle to guarantee a meaningful comparison. The results are analysed in 
Matlab,  R  and  GIS-tools  to  identify  differences  regarding  precision  and  accuracy.  The 
analyse of state of the art GNSS methods and concepts provide a benefit for scientific 
researches and users of GPS devices. This research will be a valuable resource regarding 
the definition of limitations and potentials of use cases for an individual GNSS concept.  
